import { createContext } from "react";
import { GlobalProps } from "./public/Assets/Types/types";

const contextInitialValues: GlobalProps = {
  activeIndex: 1,
};

const AppContext = createContext<GlobalProps>(contextInitialValues);

export default AppContext;
