import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SlideSelector = ({ closeSlideSelector, slidesData, goToSlide }) => {
  return (
    <div
      className="absCenter w-[95vw] h-[85vh] bg-black bg-opacity-80 grid-cols-5 p-10 z-50 rounded-xl opacity-0 cursor-default hidden overflow-scroll animate-fadeIn"
      id="slidesSelector"
    >
      {slidesData.map(
        ({ slideTitle, slideIndex, fileIndex, slideFrame, frameIndex }, i) => {
          return (
            <div
key={i}
              onClick={(e) => {
                goToSlide(e, fileIndex, slideFrame, frameIndex);
              }}
              className="text-center p-4 py-8 rounded-lg hover:bg-white hover:bg-opacity-10 duration-200 h-fit"
            >
              <h2 className="text-[#F78F1E] text-2xl font-bold">
                {i+1}
              </h2>
              <p className="text-white text-base font-medium h-full flex items-center justify-center">{slideTitle}</p>
            </div>
          );
        }
      )}

      <button
        className="absolute top-4 right-4 text-[#F78F1E] text-4xl z-50"
        onClick={closeSlideSelector}
      >
        <FontAwesomeIcon icon={faTimes} />
      </button>
    </div>
  );
};

export default SlideSelector;
