import { useEffect, useState } from "react";
import AppContext from "../AppContext";
import { GlobalProps } from "../public/Assets/Types/types";
import "../styles/globals.css";
import Script from "next/script";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  // Global Props Context
  let contextInitialValues: GlobalProps = {
    activeIndex: -1,
  };
  let [globalInfo, setGlobalInfo] = useState<GlobalProps>(contextInitialValues);
  // Fetch Global value then update the context
  useEffect(() => {
    // Fetch
// <Script src="https://unpkg.com/@lottiefiles/lottie-player@1/dist/lottie-player.js" />
    let fetchedGlobalProps: GlobalProps = {
      activeIndex: -1,
    };
    setGlobalInfo(fetchedGlobalProps);
  }, []);

  return (
    <AppContext.Provider value={globalInfo}>
      
<Script src="/Assets/lottie.js" />
      <Head>
        <title>Almarsad Presentation</title>
      </Head>
      <main
        className="cursor-pointer"
        // onClick={() => {
        //   setGlobalInfo({ activeIndex: globalInfo.activeIndex + 1 });
        // }}
      >
        <Component {...pageProps} />
      </main>
    </AppContext.Provider>
  );
}

export default MyApp;
