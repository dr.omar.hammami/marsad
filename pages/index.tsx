import { useEffect, useMemo, useRef, useState } from "react";
import "@lottiefiles/lottie-interactivity";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faTimes, faCircleHalfStroke } from "@fortawesome/free-solid-svg-icons";
import SlideSelector from "Components/SlideSelector";

const filesData = [
  // ***************************
  {
    id: 0,
    filePath: "/Assets/LottieAnimation/01.json",
    stopFrames: [
      0, 58, 104, 146, 174, 200, 252, 296, 324, 350, 376, 475, 575, 710, 784, 856, 958, 1038, 1102, 1162, 1236, 1290, 1350, 1406, 1452, 1504, 1564, 1638, 1740, 1812, 1930, 1954, 1986, 2028, 2062, 2099, 2162, 2221, 2288, 2342, 2424, 2500,
      2634, 2680, 2738, 2766, 2852, 2900,
    ],
    initialFrame: null,
    initialFrameIndex: 0,
    isBack: false,
  },
  {
    id: 1,
    filePath: "/Assets/LottieAnimation/02.json",
    stopFrames: [0, 58, 97, 137, 181, 210, 253, 290, 334, 360, 399, 437, 474, 524, 537, 550, 566, 579, 612, 659, 720, 773, 812, 855, 904],
    initialFrame: null,
    initialFrameIndex: 0,
    isBack: false,
  },
  {
    id: 2,
    filePath: "/Assets/LottieAnimation/03.json",
    stopFrames: [0, 28 + 10, 112 + 10, 183 + 10, 241 + 10, 309 + 10, 385 + 10, 441 + 10, 482 + 10, 525 + 10, 578 + 10, 627 + 10, 671 + 10, 740 + 10, 809 + 10, 863 + 10, 900 + 10],
    initialFrame: null,
    initialFrameIndex: 0,
    isBack: false,
  },
];

let slidesData = [


{ slideTitle: "السياق التنموي لمشروع تأسيس منظومة ذكية لمعلومات سوق العمل", slideIndex: 1, frameIndex:1, fileIndex:0, slideFrame:58 },
{ slideTitle: "خلفية الجهود الحكومية المبذولة عبر عقدين لتنظيم سوق العمل", slideIndex: 12, frameIndex:12, fileIndex:0, slideFrame:575 },
{ slideTitle: "ما هي المنصة؟", slideIndex: 17, frameIndex:17, fileIndex:0, slideFrame:1038 },
{ slideTitle: "ما الحاجة .. أو الضرورة لوجود منصة لمعلومات سوق العمل في سورية؟", slideIndex: 32, frameIndex:23, fileIndex:0, slideFrame:1406 },
{ slideTitle: "الأهداف", slideIndex: 36, frameIndex:36, fileIndex:0, slideFrame:2162 },
{ slideTitle: "مهام ووظائف المنصة وفوائدها", slideIndex: 41, frameIndex:41, fileIndex:0, slideFrame:2500 },



{ slideTitle: "الفوائد", slideIndex:1 , frameIndex:1, fileIndex:1, slideFrame:58 },
{ slideTitle: "الفوائد على المستوى السياساتي الكلي:", slideIndex:2 , frameIndex:2, fileIndex:1, slideFrame:97 },
{ slideTitle: "الفوائد على مستوى الخدمات التي تقدمها المنصة:", slideIndex:13 , frameIndex:13, fileIndex:1, slideFrame:524 },
{ slideTitle: "المخرجات المتوقعة من منصة المعلومات", slideIndex:18 , frameIndex:18, fileIndex:1, slideFrame:612 },
{ slideTitle: "أصحاب المصلحة والشركاء والمستفيدين", slideIndex:22 , frameIndex:22, fileIndex:1, slideFrame:812 },

{ slideTitle: "الجهات المشاركة عبر ممثلين عنها من المعنيين", slideIndex:1 , frameIndex:1, fileIndex:2, slideFrame:28 },
{ slideTitle: "مراحل العمل", slideIndex:2 , frameIndex:2, fileIndex:2, slideFrame:112 },
{ slideTitle: "الدروس المستفادة من التجارب الدولية", slideIndex:9 , frameIndex:9, fileIndex:2, slideFrame:525 },
{ slideTitle: "الصعوبات والتحديات المتوقعة", slideIndex:15 , frameIndex:15, fileIndex:2, slideFrame:863 },




];

// {
//   id: 1,
//   filePath: "/Assets/LottieAnimation/official1.json",
//   stopFrames: [0, 64, ],
//   initialFrame: null,
//   initialFrameIndex: 0,
//   isBack: false,
// },

const Index = () => {
  let oneFramesPlayed = 1;
  let [activeFile, setActiveFile] = useState(filesData[0]);
  let lottieRef = useRef<HTMLElement>();
  let [lottieInstance, setLottieInstance] = useState(null);
  let currentFileIndex = activeFile.id;
  let currentFrameIndex = activeFile.isBack ? activeFile.stopFrames.length - 2 : activeFile.id == 0 ? 0 : 1;

  let playNextSegmants = () => {
    console.log("forward");
    let lottieElement = document.getElementById(`lottiePlayer-${activeFile.id}`) as any;
    const lottieInstance = lottieElement.getLottie();
    lottieElement.setSpeed(1);

    if (lottieInstance?.isPaused) {
      oneFramesPlayed = 1;

      if (currentFrameIndex >= activeFile.stopFrames.length - 1) {
        if (!filesData[currentFileIndex + 1]) return;
        setActiveFile(filesData[currentFileIndex + 1]);
        currentFrameIndex = 0;
        lottieElement = document.getElementById(`lottiePlayer-${filesData[currentFileIndex + 1].id}`) as any;
        const lottieInstance = lottieElement.getLottie();
        console.log([filesData[currentFileIndex + 1].stopFrames[0], filesData[currentFileIndex + 1].stopFrames[1]]);
        lottieInstance?.playSegments([filesData[currentFileIndex + 1].stopFrames[0], filesData[currentFileIndex + 1].stopFrames[1]], true);
        currentFrameIndex++;
        return;
      } else {
        console.log([activeFile.stopFrames[currentFrameIndex], activeFile.stopFrames[currentFrameIndex + 1]]);
        lottieInstance?.playSegments([activeFile.stopFrames[currentFrameIndex], activeFile.stopFrames[currentFrameIndex + 1]], true);
        currentFrameIndex++;
      }
    }
  };

  let goBack = (e) => {
    console.log("back");

    e.stopPropagation();
    let lottieElement = document.getElementById(`lottiePlayer-${activeFile.id}`) as any;
    const lottieInstance = lottieElement.getLottie();
    if (lottieInstance.isPaused) {
      lottieElement.setSpeed(10);
      if (currentFrameIndex == 0) {
        if (!filesData[currentFileIndex - 1]) return;
        setActiveFile({ ...filesData[currentFileIndex - 1], isBack: true });
        currentFrameIndex = filesData[currentFileIndex - 1].stopFrames.length - 1;
        lottieElement = document.getElementById(`lottiePlayer-${filesData[currentFileIndex - 1].id}`) as any;
        const lottieInstance = lottieElement.getLottie();
        console.log([filesData[currentFileIndex - 1].stopFrames[filesData[currentFileIndex - 1].stopFrames.length - 1], filesData[currentFileIndex - 1].stopFrames[filesData[currentFileIndex - 1].stopFrames.length - 2]]);
        lottieInstance?.playSegments(
          [filesData[currentFileIndex - 1].stopFrames[filesData[currentFileIndex - 1].stopFrames.length - 1], filesData[currentFileIndex - 1].stopFrames[filesData[currentFileIndex - 1].stopFrames.length - 2]],
          true
        );
        currentFrameIndex--;
        return;
      } else {
        console.log([activeFile.stopFrames[currentFrameIndex], activeFile.stopFrames[currentFrameIndex - 1]]);
        lottieInstance?.playSegments([activeFile.stopFrames[currentFrameIndex], activeFile.stopFrames[currentFrameIndex - 1]], true);
        currentFrameIndex--;
      }
    }
  };

  let goToSlide = (e, fileIndex, frame, frameIndex) => {
    e.stopPropagation();
    if (lottieInstance.isPaused) {
      let lottieElement = document.getElementById(`lottiePlayer-${activeFile.id}`) as any;
      if (fileIndex == activeFile.id) {
        lottieElement.setSpeed(20);
        lottieInstance?.playSegments([activeFile.stopFrames[currentFrameIndex], activeFile.stopFrames[frameIndex]], true);
        currentFrameIndex = frameIndex;
      } else {
        setActiveFile({
          ...filesData[fileIndex],
          initialFrame: frame,
          initialFrameIndex: frameIndex,
        });
      }
    }
  };

  if (activeFile.initialFrame && activeFile.initialFrame != 0) {
    let lottieElement = document.getElementById(`lottiePlayer-${activeFile.id}`) as any;
const lottieInstance = lottieElement.getLottie();

    lottieElement.setSpeed(10);
    lottieInstance?.playSegments([activeFile.stopFrames[0], activeFile.stopFrames[activeFile.initialFrameIndex]], true);
    currentFrameIndex = activeFile.initialFrameIndex;
  }
  let handleKeyPress = (e: KeyboardEvent) => {

    if (e.code == "ArrowRight" || e.code == "Space" || e.key == "PageUp") {
      let lottieContainer = document.getElementById("lottiePlayerContainer");
      lottieContainer.click();
    } else if (e.code == "ArrowLeft" || e.key == "PageDown") {
      let lottieBackButton = document.getElementById("lottieBackButton");
      lottieBackButton.click();
    }
  };
  if (lottieInstance != null) {
    window.addEventListener("keydown", handleKeyPress);
  }

  useEffect(() => {
    lottieRef.current.addEventListener("load", () => {
      let lottieElement = document.getElementById(`lottiePlayer-${activeFile.id}`) as any;
      setLottieInstance(lottieElement.getLottie());
      let loadingContainer = document.getElementById("loadingContainer") as HTMLElement;

      loadingContainer.style.opacity = "0";
      loadingContainer.style.visibility = "hidden";
      loadingContainer.style.pointerEvents = "none";
    });
    let lottieContainer = document.getElementById("lottiePlayerContainer") as HTMLElement;
    // if (activeFile.initialFrame != null) {
    //   let lottieElement = document.getElementById("lottiePlayer") as any;
    // }

    lottieContainer.addEventListener("keypress", handleKeyPress);

    return () => lottieContainer.removeEventListener("keypress", handleKeyPress);
  }, [activeFile]);

  function openSlideSelector(e) {
    e.preventDefault();
    e.stopPropagation();
    let slidesSelector = document.getElementById("slidesSelector") as HTMLElement;
    slidesSelector.style.display = "grid";
  }

  function closeSlideSelector(e) {
    e.stopPropagation();
    e.preventDefault();
    let slidesSelector = document.getElementById("slidesSelector") as HTMLElement;
    slidesSelector.style.display = "none";
  }

  let playNextFrame = () => {
    let lottieElement = document.getElementById("lottiePlayer") as any;
    console.log("The Addedd Frames Is: " + oneFramesPlayed);
    console.log("The Frame Is: " + activeFile.stopFrames[currentFrameIndex] + oneFramesPlayed);
    lottieElement.setSpeed(1);
    if (lottieInstance?.isPaused) {
      oneFramesPlayed++;
      lottieInstance?.playSegments([activeFile.stopFrames[currentFrameIndex] + oneFramesPlayed - 1, activeFile.stopFrames[currentFrameIndex] + oneFramesPlayed]);
    }
  };

  return (
    <div className="h-screen w-screen relative">
      {/* <button
        onClick={playNextFrame}
        className="z-[9999999999999999999999999999999999] cursor-pointer absolute top-0 right-40 hover:scale-110 duration-200"
      >
        playNextFrame
      </button> */}
      <div className="h-screen w-screen flexCenter cursor-pointer relative" onClick={playNextSegmants} id="lottiePlayerContainer">
        {filesData.map(({ filePath, id, initialFrame, initialFrameIndex, isBack, stopFrames }) => {
          return (
            <div className={`${activeFile.id != id && "hidden"}`} key={id}>
              <lottie-player
                mode="normal"
                src={filePath}
                id={`lottiePlayer-${id}`}
                speed={1}
                // autoplay
                ref={lottieRef}
              ></lottie-player>
            </div>
          );
        })}

        {/* Second */}
        {/* {[filesData[activeFile.id]].map(({ filePath, id, initialFrame, initialFrameIndex, isBack, stopFrames }) => {
          return (
            <lottie-player
              key={id}
              mode="normal"
              src={filePath}
              id="lottiePlayer"
              speed={1}
              // autoplay
              ref={lottieRef}
            ></lottie-player>
          );
        })} */}

        {/* First */}
        {/* <lottie-player
          mode="normal"
          src={activeFile.filePath}
          id="lottiePlayer"
          speed={1}
          // autoplay
          ref={lottieRef}
        ></lottie-player> */}
      </div>
      <div className="flex items-center gap-8 justify-center absolute top-0 left-0 w-28 aspect-square group cursor-default">
        <button
          id="lottieBackButton"
          onClick={goBack}
          className="w-12 h-12 aspect-square rounded-full flex items-center justify-center bg-white text-black shadow-xl text-2xl absolute top-1/2 -translate-y-1/2 right-full group-hover:right-1/2 group-hover:translate-x-1/2 duration-300"
        >
          <FontAwesomeIcon icon={faArrowLeft} />
        </button>
      </div>
       <button className="absolute top-5 right-5 flex items-center justify-center bg-[#F78F1E] w-14 aspect-square rounded-md" onClick={openSlideSelector}>
        <SlidesIcon />
      </button>
      <SlideSelector closeSlideSelector={closeSlideSelector} slidesData={slidesData} goToSlide={goToSlide} />
      <div className="absCenter w-full h-full flex items-center justify-center bg-white z-[100000] duration-300 delay-500" id="loadingContainer">
        <div className="relative">
          <div className="scale-[25%] absolute left-1/2 top-[200%] -translate-x-1/2">
            <LoadingCircle />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;

const SlidesIcon = () => {
  return (
    <svg width="32" height="27" viewBox="0 0 32 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1.57129 1H30.1427" stroke="#ff253a" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      <path d="M4.42847 1H27.2856V19.5714H4.42847V1Z" stroke="#ff253a" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
      <path d="M20.857 8.14285L22.9998 10.2857L20.857 12.4286M10.857 12.4286L8.71411 10.2857L10.857 8.14285M10.1427 25.2857L15.857 19.5714L21.5713 25.2857" stroke="#ff253a" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    </svg>
  );
};

const LoadingCircle = () => {
  return (
    <div id="wrapper">
      <div className="profile-main-loader">
        <div className="loader">
          <svg className="circular-loader" viewBox="25 25 50 50">
            <circle className="loader-path" cx="50" cy="50" r="20" fill="none" stroke="#193264" stroke-width="2" />
          </svg>
        </div>
      </div>
    </div>
  );
};
